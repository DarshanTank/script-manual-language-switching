from pymongo import MongoClient
import requests
import json


def hit_bot(ph_no,name,flow_id):

    url = "http://172.16.22.6:8989/originate/outbound"

    payload = json.dumps({
    "phone": str(ph_no),
    "flow_id": str(flow_id),
    "name": str(name)
    })
    headers = {
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    j = json.loads(response.text)
    return j

def connect(name_of_DB,name_of_collection):
    connection_string= "mongodb://root:root@172.16.22.5:27019/?authSource=admin&readPreference=primary&directConnection=true&ssl=false"  
    try:
        conn = MongoClient(connection_string)
        print("Connected successfully!!!")
    except:  
        print("Could not connect to MongoDB")
    
    db = conn[name_of_DB]

    # Created or Switched to collection names: myTable
    collection = db[name_of_collection]
    return collection
